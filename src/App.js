import './App.css';
import Todo from './store/features/todo/Todo';

function App() {
  return (
    <div className="App">
      <Todo />
    </div>
  );
}

export default App;