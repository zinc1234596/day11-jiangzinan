import {useDispatch} from "react-redux";
import {switchTodoCompleteStatus, removeTodo} from "./todoSlice";

const TodoItem = (props) => {
    const dispatch = useDispatch();
    const handleSwitchTodoCompleteStatus = () => {
        dispatch(switchTodoCompleteStatus(todo.id))
    }
    const handleRemoveTodo = () => {
        dispatch(removeTodo(todo.id))
    }
    const {todo} = props;
    return (
        <li>
            <div style={{display: 'flex', justifyContent: 'space-between'}}>
                <div style={{cursor: 'pointer', textDecoration: todo.isComplete ? "line-through" : "none"}}
                     onClick={handleSwitchTodoCompleteStatus}>{todo.todoTitle}</div>
                <button onClick={handleRemoveTodo}>❌</button>
            </div>
        </li>
    );
}

export default TodoItem;