import { useState } from 'react';
import {useDispatch} from "react-redux";
import {addTodo} from "./todoSlice";
const TodoForm = () => {
    const dispatch = useDispatch();
    const [todoTitle, setTodoTitle] = useState('');

    const handleSubmit = e => {
        e.preventDefault();
        dispatch(addTodo(todoTitle));
        setTodoTitle('');
    }

    return (
        <form onSubmit={handleSubmit}>
            <input
                type="text"
                value={todoTitle}
                onChange={e => setTodoTitle(e.target.value)}
            />
            <button type="submit">Add</button>
        </form>
    )
}

export default TodoForm;