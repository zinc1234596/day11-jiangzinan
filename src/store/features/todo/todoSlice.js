import {createSlice} from "@reduxjs/toolkit";
import * as nanoid from "nanoid";

export const todoSlice = createSlice({
    name: 'todo',
    initialState: {
        todos: []
    },
    reducers: {
        addTodo: (state, action) => {
            const todoTitle = action.payload;
            if (!todoTitle) return;
            state.todos.push({id: nanoid.nanoid(), todoTitle, isComplete: false})
        },
        removeTodo: (state, action) => {
            const id = action.payload;
            const index = state.todos.findIndex(todo => todo.id === id);
            state.todos.splice(index, 1);
        },
        switchTodoCompleteStatus: (state, action) => {
            const id = action.payload;
            const todoToUpdate = state.todos.find(todo => todo.id === id);
            if (todoToUpdate) {
                todoToUpdate.isComplete = !todoToUpdate.isComplete;
            }
        }
    }
})

export const {addTodo, removeTodo, switchTodoCompleteStatus} = todoSlice.actions

export default todoSlice.reducer;