### O:

- Today, we learned Redux, which was relatively smooth to grasp since we had previously learned Vuex and Pinia.
- During our free time today, i explored how Redux handles asynchronous operations. We discovered that we can use `createAsyncThunk` to define an asynchronous operation, which automatically generates the corresponding Redux action and reducer.

### R:

- Today's overall progress is smooth.

### I:

- In frameworks like React and Vue, it is often necessary to use `require` to import images because of bundlers like Vite and Webpack. During compilation, the image path is treated as a regular string instead of a usable image reference. As a result, the browser is unable to correctly interpret the path and load the corresponding image. This was a point that I had not previously delved into the underlying principles of.

### D:

- Keep my enthusiasm for learning